import { RootCommandData, OptionData, ArgumentData } from '../interfaces';
import Command from './Command';
export default class RootCommand {
    appData: RootCommandData;
    constructor(appName: string);
    name(name: string): this;
    description(desc: string): this;
    license(license: string): this;
    website(link: string): this;
    version(ver: string, showFlag?: boolean, func?: () => void): this;
    argument(arg: ArgumentData): this;
    option(option: OptionData): this;
    command(name: string, commandFn: (command: Command) => Command): this;
    synopsis(synopsis: string): this;
    func(func: (rootCommand: RootCommandData, globalOptions: OptionData[]) => void): this;
}
//# sourceMappingURL=RootCommand.d.ts.map