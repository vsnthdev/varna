import { CommandData, OptionData, ArgumentData, RootCommandData } from '../interfaces';
export default class Command {
    appData: RootCommandData;
    commandData: CommandData;
    constructor(name: string, appData: RootCommandData);
    func(func: (command: CommandData, commandOptions: OptionData[], globalOptions: OptionData[]) => void): this;
    description(desc: string): this;
    argument(arg: ArgumentData): this;
    option(option: OptionData): this;
    subCommand(name: string, commandFn: (command: Command) => Command): this;
    synopsis(synopsis: string): this;
}
//# sourceMappingURL=Command.d.ts.map