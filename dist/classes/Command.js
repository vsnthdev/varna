"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var options_1 = __importDefault(require("../options"));
var index_1 = __importDefault(require("../help/index"));
var commands_1 = __importDefault(require("../commands"));
var arguments_1 = __importDefault(require("../arguments"));
var Command = (function () {
    function Command(name, appData) {
        var _this = this;
        this.appData = appData;
        this.commandData = {
            name: name,
            description: null,
            options: [],
            synopsis: [],
            subCommands: [],
            func: function () {
                (0, index_1.default)(_this.appData, _this.commandData);
                process.exit(0);
            },
            argument: {
                multiple: false,
                type: null,
                value: null,
            },
        };
    }
    Command.prototype.func = function (func) {
        this.commandData.func = func;
        return this;
    };
    Command.prototype.description = function (desc) {
        this.commandData.description = desc;
        return this;
    };
    Command.prototype.argument = function (arg) {
        arguments_1.default.addArgument(arg, this.commandData);
        return this;
    };
    Command.prototype.option = function (option) {
        options_1.default.addOption(option, this.commandData);
        return this;
    };
    Command.prototype.subCommand = function (name, commandFn) {
        commands_1.default.addCommand(this.appData, name, commandFn, this.commandData);
        return this;
    };
    Command.prototype.synopsis = function (synopsis) {
        var newSynopsis = {
            id: this.commandData.synopsis.length + 1,
            text: synopsis,
        };
        this.commandData.synopsis.push(newSynopsis);
        return this;
    };
    return Command;
}());
exports.default = Command;
//# sourceMappingURL=Command.js.map