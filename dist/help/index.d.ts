import { RootCommandData, CommandData } from '../interfaces';
export default function showHelp(appData: RootCommandData, command: CommandData, short?: boolean): void;
//# sourceMappingURL=index.d.ts.map