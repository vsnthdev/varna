"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function makeHeader(appData, styles) {
    if (!appData.version.value)
        return '';
    return ("" + styles.newLine + styles.header('USAGE') + " ")
        .concat(styles.newIndentedLine)
        .concat(styles.name(appData.name) + " ")
        .concat("" + (appData.commands.length > 0
        ? "<" + styles.command('command') + "> "
        : ''))
        .concat("[" + styles.option('options') + "]")
        .concat(appData.description
        ? "\n" + styles.newIndentedLine + appData.description
        : '');
}
exports.default = makeHeader;
//# sourceMappingURL=header.js.map