"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var wrap_ansi_1 = __importDefault(require("wrap-ansi"));
var cli_table3_1 = __importDefault(require("cli-table3"));
function makeOptions(appData, options, styles, short) {
    if (options.length == 0)
        return '';
    var table = new cli_table3_1.default({
        chars: {
            top: '',
            'top-mid': '',
            'top-left': '',
            'top-right': '',
            bottom: '',
            'bottom-mid': '',
            'bottom-left': '',
            'bottom-right': '',
            left: '',
            'left-mid': '',
            mid: '',
            'mid-mid': '',
            right: '',
            'right-mid': '',
            middle: ' ',
        },
        style: { 'padding-left': 3, 'padding-right': 0 },
    });
    options.forEach(function (option, index) {
        if (short == true) {
            if (index < 5) {
                var first = "" + (option.alias ? styles.option("-" + option.alias) + ", " : '') + styles.option("--" + option.name) + (option.type != Boolean
                    ? " <" + styles.typeLabel(option.typeLabel
                        ? option.typeLabel
                        : option.type.name.toLowerCase()) + ">"
                    : '');
                var second = option.description
                    ? (0, wrap_ansi_1.default)(option.description, appData.config.maxWidth)
                    : '     ';
                var third = option.value
                    ? styles.defaultValue("default: " + option.value)
                    : '';
                table.push([first, second, third]);
            }
        }
        else {
            var first = "" + (option.alias ? styles.option("-" + option.alias) + ", " : '') + styles.option("--" + option.name) + (option.type != Boolean
                ? " <" + styles.typeLabel(option.typeLabel
                    ? option.typeLabel
                    : option.type.name.toLowerCase()) + ">"
                : '');
            var second = option.description
                ? (0, wrap_ansi_1.default)(option.description, appData.config.maxWidth)
                : '     ';
            var third = option.value
                ? styles.defaultValue("default: " + option.value)
                : '';
            table.push([first, second, third]);
        }
    });
    if (short == true && options.length > 5) {
        table.push([styles.option('...'), '   ', '   ']);
    }
    var printable = [];
    table
        .toString()
        .split('\n')
        .forEach(function (line) {
        printable.push(' '.repeat(appData.config.indentation) + line.trim() + '\n');
    });
    return ("\n" + styles.newLine + (options == appData.options
        ? styles.header('GLOBAL OPTIONS')
        : styles.header('OPTIONS')))
        .concat('\n')
        .concat(printable.join(''));
}
exports.default = makeOptions;
//# sourceMappingURL=options.js.map