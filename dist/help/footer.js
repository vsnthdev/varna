"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function makeFooter(appData, styles) {
    if (appData.license == null && appData.website == null)
        return '';
    return ("" + styles.newIndentedLine)
        .concat(appData.license
        ? "Licensed under " + styles.license(appData.license)
        : '')
        .concat(styles.newIndentedLine)
        .concat(appData.website ? "Visit " + styles.website(appData.website) : '');
}
exports.default = makeFooter;
//# sourceMappingURL=footer.js.map