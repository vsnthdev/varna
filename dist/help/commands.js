"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var cli_table3_1 = __importDefault(require("cli-table3"));
function makeShortCommand(cmd) {
    if (cmd.subCommands.length > 0) {
        cmd.subCommands[0].name = cmd.name + ' ' + cmd.subCommands[0].name;
        return makeShortCommand(cmd.subCommands[0]);
    }
    else {
        return cmd;
    }
}
function makeCommands(appData, commands, styles, short) {
    if (commands.length == 0)
        return '';
    var table = new cli_table3_1.default({
        chars: {
            top: '',
            'top-mid': '',
            'top-left': '',
            'top-right': '',
            bottom: '',
            'bottom-mid': '',
            'bottom-left': '',
            'bottom-right': '',
            left: '',
            'left-mid': '',
            mid: '',
            'mid-mid': '',
            right: '',
            'right-mid': '',
            middle: ' ',
        },
        style: { 'padding-left': 3, 'padding-right': 0 },
    });
    commands.forEach(function (cmd, index) {
        if (short == true) {
            if (index < 4) {
                var command = makeShortCommand(cmd);
                table.push([
                    styles.command(command.name) + " " + (command.argument.type
                        ? "" + (command.argument.value ? '[' : '<') + (command.argument.typeLabel
                            ? styles.typeLabel(command.argument.typeLabel.toLowerCase())
                            : styles.typeLabel(command.argument.type.name
                                .toString()
                                .toLowerCase())) + (command.argument.value ? ']' : '>')
                        : ''),
                    command.description,
                    command.argument.value
                        ? styles.defaultValue("default: " + command.argument.value)
                        : '   ',
                ]);
            }
        }
        else {
            table.push([
                styles.command(cmd.name) + " " + (cmd.argument.type
                    ? "" + (cmd.argument.value ? '[' : '<') + (cmd.argument.typeLabel
                        ? styles.typeLabel(cmd.argument.typeLabel.toLowerCase())
                        : styles.typeLabel(cmd.argument.type.name
                            .toString()
                            .toLowerCase())) + (cmd.argument.value ? ']' : '>')
                    : ''),
                cmd.description,
                cmd.argument.value
                    ? styles.defaultValue("default: " + cmd.argument.value)
                    : '   ',
            ]);
        }
    });
    if (short == true && commands.length > 4) {
        table.push([styles.command('...'), '   ']);
    }
    var printable = [];
    table
        .toString()
        .split('\n')
        .forEach(function (line) {
        printable.push(' '.repeat(appData.config.indentation) + line.trim() + '\n');
    });
    return ("\n" + styles.newLine + styles.header('COMMANDS'))
        .concat('\n')
        .concat(printable.join('').slice(0, -1));
}
exports.default = makeCommands;
//# sourceMappingURL=commands.js.map