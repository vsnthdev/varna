import { RootCommandData, OptionData } from '../interfaces';
export default function makeOptions(appData: RootCommandData, options: OptionData[], styles: any, short: boolean): string;
//# sourceMappingURL=options.d.ts.map