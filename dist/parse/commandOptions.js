"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var globalOptions_1 = require("./globalOptions");
function parseCommandOptions(appData, command, sourceOptions) {
    var commandOptions = [];
    var cleanUpStrings = [];
    var options = command ? command.options : appData.options;
    var _loop_1 = function (index) {
        if (sourceOptions[index].toString().startsWith('--')) {
            var found = options.find(function (option) {
                return option.name == sourceOptions[index].toString().substring(2);
            });
            if (found) {
                var parsed = (0, globalOptions_1.parseSingleOption)(found, index, sourceOptions);
                commandOptions.push(parsed);
                if (found.type == Boolean) {
                    cleanUpStrings.push(sourceOptions[index]);
                }
                else {
                    cleanUpStrings.push(sourceOptions[index], sourceOptions[index + 1]);
                }
            }
            else {
                throw Error("Unrecognized option \"" + sourceOptions[index] + "\".");
            }
        }
        else if (sourceOptions[index].toString().startsWith('-')) {
            var found = options.find(function (option) {
                return option.alias ==
                    sourceOptions[index].toString().substring(1);
            });
            if (found) {
                var parsed = (0, globalOptions_1.parseSingleOption)(found, index, sourceOptions);
                commandOptions.push(parsed);
                if (found.type == Boolean) {
                    cleanUpStrings.push(sourceOptions[index]);
                }
                else {
                    cleanUpStrings.push(sourceOptions[index], sourceOptions[index + 1]);
                }
            }
            else {
                throw Error("Unrecognized option \"" + sourceOptions[index] + "\".");
            }
        }
    };
    for (var index = 0; index < sourceOptions.length; index++) {
        _loop_1(index);
    }
    cleanUpStrings.forEach(function (skipStr) {
        sourceOptions.splice(sourceOptions.indexOf(skipStr), 1);
    });
    return (0, globalOptions_1.populateOptionsArray)(options, commandOptions);
}
exports.default = parseCommandOptions;
//# sourceMappingURL=commandOptions.js.map