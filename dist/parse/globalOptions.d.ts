import { RootCommandData, OptionData } from '../interfaces';
export declare function parseSingleOption(option: OptionData, index: number, sourceOptions: string[]): OptionData;
export declare function getValOfOption(existsAt: number, sourceOptions: string[], cmdName?: string): string | undefined;
export declare function populateOptionsArray(options: OptionData[], optionsToBePopulated: OptionData[]): OptionData[];
export default function parseGlobalOptions(appData: RootCommandData, sourceOptions: string[]): OptionData[];
//# sourceMappingURL=globalOptions.d.ts.map