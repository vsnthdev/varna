import { CommandData, RootCommandData, OptionData } from '../interfaces';
export default function parseCommandOptions(appData: RootCommandData, command: CommandData, sourceOptions: string[]): OptionData[];
//# sourceMappingURL=commandOptions.d.ts.map