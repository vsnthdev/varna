"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function addOption(option, command) {
    if (!option)
        throw Error('An option was not passed.');
    if (!option.name)
        throw Error('A name for the option is required.');
    if (!option.type)
        throw Error('An option should have a type.');
    if (option.alias && option.alias.length > 1)
        throw Error("The alias \"" + option.alias + "\" for option \"" + option.name + "\" is more than 1 character.");
    if (option.alias && option.alias.length < 1)
        throw Error("The alias for option \"" + option.name + "\" is empty.");
    var nameExists = command.options.find(function (argv) { return argv.name == option.name; });
    var aliasExists = command.options.find(function (argv) { return argv.alias == option.alias; });
    if (nameExists)
        throw Error("An option with name \"" + option.name + "\" was already exists on root.");
    if (aliasExists)
        throw Error("The alias \"" + option.alias + "\" was already assigned to \"" + aliasExists.name + "\" on root.");
    command.options.push(option);
    var help = command.options.find(function (option) { return option.name == 'help'; });
    var version = command.options.find(function (option) { return option.name == 'version'; });
    if (version)
        command.options.push(command.options.splice(command.options.indexOf(version), 1)[0]);
    if (help)
        command.options.push(command.options.splice(command.options.indexOf(help), 1)[0]);
}
exports.default = {
    addOption: addOption,
};
//# sourceMappingURL=options.js.map