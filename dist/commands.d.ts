import Command from './classes/Command';
import { RootCommandData, CommandData } from './interfaces';
declare function addCommand(appData: RootCommandData, name: string, commandFn: (command: Command) => Command, command: RootCommandData | CommandData): void;
declare const _default: {
    addCommand: typeof addCommand;
};
export default _default;
//# sourceMappingURL=commands.d.ts.map