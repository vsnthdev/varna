import {
    CommandData,
    OptionData,
    ArgumentData,
    SynopsisData,
    RootCommandData,
} from '../interfaces'
import options from '../options'
import showHelp from '../help/index'
import commands from '../commands'
import args from '../arguments'

export default class Command {
    appData: RootCommandData
    commandData: CommandData

    constructor(name: string, appData: RootCommandData) {
        // take note of the parent appData
        this.appData = appData

        // initialize a new CommandData object
        this.commandData = {
            name,
            description: null,
            options: [],
            synopsis: [],
            subCommands: [],
            func: () => {
                showHelp(this.appData, this.commandData)
                process.exit(0)
            },
            argument: {
                multiple: false,
                type: null,
                value: null,
            },
        }
    }

    func(
        func: (
            command: CommandData,
            commandOptions: OptionData[],
            globalOptions: OptionData[],
        ) => void,
    ): this {
        this.commandData.func = func
        return this
    }

    description(desc: string): this {
        this.commandData.description = desc
        return this
    }

    argument(arg: ArgumentData): this {
        args.addArgument(arg, this.commandData)
        return this
    }

    option(option: OptionData): this {
        options.addOption(option, this.commandData)
        return this
    }

    subCommand(name: string, commandFn: (command: Command) => Command): this {
        commands.addCommand(this.appData, name, commandFn, this.commandData)
        return this
    }

    synopsis(synopsis: string): this {
        const newSynopsis: SynopsisData = {
            id: this.commandData.synopsis.length + 1,
            text: synopsis,
        }
        this.commandData.synopsis.push(newSynopsis)
        return this
    }
}
