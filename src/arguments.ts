import { ArgumentData, RootCommandData, CommandData } from './interfaces'

function addArgument(
    arg: ArgumentData,
    command: RootCommandData | CommandData,
): void {
    // check if all the required variables are provided
    if (!arg) throw Error('No argument object was passed.')
    if (!arg.type) throw Error('An argument type is required.')
    if (arg.type == Boolean)
        throw Error('A positional argument cannot be of type boolean.')

    // check if there is already an argument registered with the passed command
    if (command.argument.type)
        throw Error(
            `An argument of type ${command.argument.type.name.toString()} is already assigned to the command "${
                command.name
            }".`,
        )

    // add it!
    arg.multiple
        ? (command.argument.multiple = arg.multiple)
        : (command.argument.multiple = false)
    arg.value
        ? (command.argument.value = arg.value)
        : (command.argument.value = null)
    if (arg.typeLabel) command.argument.typeLabel = arg.typeLabel
    command.argument.type = arg.type
}

export default {
    addArgument,
}
