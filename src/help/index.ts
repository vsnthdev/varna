import chalk from 'chalk'

import { RootCommandData, CommandData } from '../interfaces'
import makeHeader from './header'
import makeCommands from './commands'
import makeOptions from './options'
import makeFooter from './footer'

function makeAdditionalBanner(
    appData: RootCommandData,
    command: CommandData,
    styles,
): string {
    const message = `See "${appData.name} ${
        command ? styles.command(command.name + ' ') : ''
    }${styles.option('--help')}" for full help.`

    if (command) {
        if (
            appData.commands.length > 4 ||
            command.subCommands.length > 4 ||
            appData.options.length > 5 ||
            command.options.length > 5
        ) {
            return `\n${styles.newIndentedLine}`.concat(message ? message : '')
        } else {
            return ''
        }
    } else {
        if (appData.commands.length > 4 || appData.options.length > 5) {
            return `\n${styles.newIndentedLine}`.concat(message ? message : '')
        } else {
            return ''
        }
    }
}

export default function showHelp(
    appData: RootCommandData,
    command: CommandData,
    short = true,
): void {
    const styles = {
        newLine: `\n${' '.repeat(appData.config.padding)}`,
        newIndentedLine: `\n${' '.repeat(
            appData.config.padding + appData.config.indentation,
        )}`,

        header: appData.config.colored ? chalk.bold.white : chalk.bold,
        name: appData.config.colored ? chalk.bold : chalk,
        license: appData.config.colored ? chalk.bold.white : chalk.bold,
        website: appData.config.colored
            ? chalk.underline.bold
            : chalk.underline.bold,
        command: appData.config.colored ? chalk.blueBright : chalk,
        option: appData.config.colored ? chalk.yellowBright : chalk,
        typeLabel: appData.config.colored ? chalk.italic.cyanBright : chalk,
        defaultValue: appData.config.colored ? chalk.grey : chalk,
    }

    const helpString = ''
        .concat(makeHeader(appData, styles))
        .concat(
            command
                ? makeCommands(appData, command.subCommands, styles, short)
                : makeCommands(appData, appData.commands, styles, short),
        )
        .concat(
            command ? makeOptions(appData, command.options, styles, short) : '',
        )
        .concat(makeOptions(appData, appData.options, styles, short))
        .concat(makeFooter(appData, styles))
        .concat(
            short == true ? makeAdditionalBanner(appData, command, styles) : '',
        )

    console.log(helpString)
}
