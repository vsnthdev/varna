import wrap from 'wrap-ansi'
import Table from 'cli-table3'

import { RootCommandData, OptionData } from '../interfaces'

export default function makeOptions(
    appData: RootCommandData,
    options: OptionData[],
    styles: any,
    short: boolean,
): string {
    if (options.length == 0) return ''

    const table = new Table({
        chars: {
            top: '',
            'top-mid': '',
            'top-left': '',
            'top-right': '',
            bottom: '',
            'bottom-mid': '',
            'bottom-left': '',
            'bottom-right': '',
            left: '',
            'left-mid': '',
            mid: '',
            'mid-mid': '',
            right: '',
            'right-mid': '',
            middle: ' ',
        },
        style: { 'padding-left': 3, 'padding-right': 0 },
    })

    options.forEach((option, index) => {
        if (short == true) {
            if (index < 5) {
                const first = `${
                    option.alias ? `${styles.option(`-${option.alias}`)}, ` : ''
                }${styles.option(`--${option.name}`)}${
                    option.type != Boolean
                        ? ` <${styles.typeLabel(
                              option.typeLabel
                                  ? option.typeLabel
                                  : option.type.name.toLowerCase(),
                          )}>`
                        : ''
                }`
                const second = option.description
                    ? wrap(option.description, appData.config.maxWidth)
                    : '     '
                const third = option.value
                    ? styles.defaultValue(`default: ${option.value}`)
                    : ''
                table.push([first, second, third])
            }
        } else {
            const first = `${
                option.alias ? `${styles.option(`-${option.alias}`)}, ` : ''
            }${styles.option(`--${option.name}`)}${
                option.type != Boolean
                    ? ` <${styles.typeLabel(
                          option.typeLabel
                              ? option.typeLabel
                              : option.type.name.toLowerCase(),
                      )}>`
                    : ''
            }`
            const second = option.description
                ? wrap(option.description, appData.config.maxWidth)
                : '     '
            const third = option.value
                ? styles.defaultValue(`default: ${option.value}`)
                : ''
            table.push([first, second, third])
        }
    })

    // add the tailing ... to notify the user that additional options
    // are available
    if (short == true && options.length > 5) {
        table.push([styles.option('...'), '   ', '   '])
    }

    const printable: string[] = []

    table
        .toString()
        .split('\n')
        .forEach(line => {
            printable.push(
                ' '.repeat(appData.config.indentation) + line.trim() + '\n',
            )
        })

    return `\n${styles.newLine}${
        options == appData.options
            ? styles.header('GLOBAL OPTIONS')
            : styles.header('OPTIONS')
    }`
        .concat('\n')
        .concat(printable.join(''))
}
