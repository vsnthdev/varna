// This file will validate if the option can be added to a command
// and adds it if everything checks out

import { OptionData, RootCommandData, CommandData } from './interfaces'

function addOption(
    option: OptionData,
    command: RootCommandData | CommandData,
): void {
    // check if the user passed an option
    // and that there is a name and a type
    if (!option) throw Error('An option was not passed.')
    if (!option.name) throw Error('A name for the option is required.')
    if (!option.type) throw Error('An option should have a type.')

    // check if there is an alias. If yes then ensure it's only 1 char[]
    if (option.alias && option.alias.length > 1)
        throw Error(
            `The alias "${option.alias}" for option "${option.name}" is more than 1 character.`,
        )
    if (option.alias && option.alias.length < 1)
        throw Error(`The alias for option "${option.name}" is empty.`)

    // check if the name and alias have been already taken
    const nameExists = command.options.find(argv => argv.name == option.name)
    const aliasExists = command.options.find(argv => argv.alias == option.alias)

    if (nameExists)
        throw Error(
            `An option with name "${option.name}" was already exists on root.`,
        )
    if (aliasExists)
        throw Error(
            `The alias "${option.alias}" was already assigned to "${aliasExists.name}" on root.`,
        )

    // add the newly created option to the options list
    command.options.push(option)

    // move help and version flags to the bottom
    const help = command.options.find(option => option.name == 'help')
    const version = command.options.find(option => option.name == 'version')

    if (version)
        command.options.push(
            command.options.splice(command.options.indexOf(version), 1)[0],
        )
    if (help)
        command.options.push(
            command.options.splice(command.options.indexOf(help), 1)[0],
        )
}

export default {
    addOption,
}
