import { CommandData, RootCommandData, OptionData } from '../interfaces'
import { parseSingleOption, populateOptionsArray } from './globalOptions'

export default function parseCommandOptions(
    appData: RootCommandData,
    command: CommandData,
    sourceOptions: string[],
): OptionData[] {
    // prepare the returnable constant that holds list of command options
    const commandOptions: OptionData[] = []
    const cleanUpStrings: string[] = []

    // check if command was null, in that case work with appData.options to parse
    // then as global options
    const options: OptionData[] = command ? command.options : appData.options

    for (let index = 0; index < sourceOptions.length; index++) {
        // TODO: as there is code duplication, put these in a separate function
        if (sourceOptions[index].toString().startsWith('--')) {
            const found = options.find(
                option =>
                    option.name == sourceOptions[index].toString().substring(2),
            )

            if (found) {
                const parsed = parseSingleOption(found, index, sourceOptions)
                commandOptions.push(parsed)

                // check it's type and act accordingly
                if (found.type == Boolean) {
                    cleanUpStrings.push(sourceOptions[index])
                } else {
                    cleanUpStrings.push(
                        sourceOptions[index],
                        sourceOptions[index + 1],
                    )
                }
            } else {
                throw Error(`Unrecognized option "${sourceOptions[index]}".`)
            }
        } else if (sourceOptions[index].toString().startsWith('-')) {
            const found = options.find(
                option =>
                    option.alias ==
                    sourceOptions[index].toString().substring(1),
            )
            if (found) {
                const parsed = parseSingleOption(found, index, sourceOptions)
                commandOptions.push(parsed)

                // check it's type and act accordingly
                if (found.type == Boolean) {
                    cleanUpStrings.push(sourceOptions[index])
                } else {
                    cleanUpStrings.push(
                        sourceOptions[index],
                        sourceOptions[index + 1],
                    )
                }
            } else {
                throw Error(`Unrecognized option "${sourceOptions[index]}".`)
            }
        }
    }

    cleanUpStrings.forEach(skipStr => {
        sourceOptions.splice(sourceOptions.indexOf(skipStr), 1)
    })

    return populateOptionsArray(options, commandOptions)
}
