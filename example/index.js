// This file will demonstrate how to use varna project.

const varna = require('../dist/varna')

async function main() {
    const app = varna.app('varna')
        .license('MIT')
        .version('12.0.2')
        .description('A simple application!')
        .website('https://github.com/vasanthdeveloper/varna')
        .option({
            name: 'count',
            alias: 'c',
            type: Number,
            value: 3
        })
        .command('command', cmd => {
            return cmd.description('An example command.')
                .subCommand('subcommand', cmd => {
                    return cmd.description('An example sub-command.')
                        .argument({
                            type: String,
                            value: 'Vasanth'
                        })
                })
        })
        .command('command2', cmd => {
            return cmd
        })

    varna.parse(app)
}

main()